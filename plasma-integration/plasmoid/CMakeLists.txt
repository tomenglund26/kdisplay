add_definitions(-DTRANSLATION_DOMAIN=\"kdisplay-plasma-integration\")

# WARNING PlasmaQuick provides unversioned CMake config
find_package(KF5 REQUIRED COMPONENTS PlasmaQuick)

set(kdisplayApplet_SRCS
    kdisplay_applet.cpp
    ../kded/osdaction.cpp
)

add_library(plasma_applet_kdisplay MODULE ${kdisplayApplet_SRCS})

kcoreaddons_desktop_to_json(plasma_applet_kdisplay package/metadata.desktop)

target_link_libraries(plasma_applet_kdisplay
                      Qt5::Qml
                      Qt5::DBus
                      KF5::I18n
                      KF5::Plasma
                      Disman::Disman)

install(TARGETS plasma_applet_kdisplay DESTINATION ${KDE_INSTALL_PLUGINDIR}/plasma/applets)

plasma_install_package(package org.kwinft.kdisplay)
